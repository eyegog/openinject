#include<openinject.h>
#include<string.h>
#include<arpa/inet.h>

int openinj_build_arp(
        openinj_frame *frame,
        uint16_t htype,
        uint16_t ptype,
        uint8_t hlen,
        uint8_t plen,
        uint16_t op,
        uint8_t *sha,
        uint8_t *spa,
        uint8_t *tha,
        uint8_t *tpa)
{

    uint8_t *ptr = frame->data;

    /* If ethernet is being used for L2 then
     * add 14 bytes to skip the ethernet frame */
    if(frame->flags.ETHER)
        ptr += 14;

    struct arp_hdr *packet_header = (struct arp_hdr *) ptr;

    /* Copy all parameters into the packet and set the
     * ARP flag */
    packet_header->htype = htons(htype);
    packet_header->ptype = htons(ptype);
    packet_header->hlen = hlen;
    packet_header->plen = plen;
    packet_header->op = htons(op);
    memcpy(packet_header->sha, sha, hlen);
    memcpy(packet_header->spa, spa, plen);
    memcpy(packet_header->tha, tha, hlen);
    memcpy(packet_header->tpa, tpa, plen);

    if(frame->flags.ARP != 1) {
        frame->flags.ARP = 1;
        frame->framelen += sizeof(struct arp_hdr);
    }

    return 0;
}
