#include<openinject.h>
#include<linux/ip.h>
#include<string.h>

#define ETH_HDR_LEN 14

int openinj_build_ipv4(openinj_frame *frame,
                       uint8_t version,
                       uint8_t ihl,
                       uint8_t tos,
                       uint16_t totlen,
                       uint16_t id,
                       uint16_t fragoff,
                       uint8_t ttl,
                       uint8_t proto,
                       uint32_t source,
                       uint32_t dest,
                       uint8_t *options,
                       int options_len)
{
    struct iphdr header;
    int header_len = sizeof(header);
    header.version = version;
    header.ihl = ihl;
    header.tos = tos;
    if(totlen == 0)
        header.tot_len = htons((frame->framelen - ETH_HDR_LEN) + header_len);
    else
        header.tot_len = htons(totlen);
    header.id = htons(id);
    header.frag_off = htons(fragoff);
    header.ttl = ttl;
    header.protocol = proto;
    /* Checksum calculation should not include
     * the checksum itself */
    header.check = 0;
    header.saddr = source;
    header.daddr = dest;

    header.check = openinj_rfc791_checksum((uint8_t *) &header, header_len);

    /* TODO: Add support for options */
    memcpy(frame->data+frame->framelen, &header, header_len);

    if(frame->flags.IP4 != 1) {
        frame->flags.IP4 = 1;
        frame->framelen += header_len;
    }

    return 0;
}

void openinj_recalc_ipv4(openinj_frame *frame)
{
    struct iphdr *header = (struct iphdr *) (frame->data + ETH_HDR_LEN);
    int header_len = sizeof(struct iphdr);
    header->tot_len = htons(frame->framelen - ETH_HDR_LEN);
    header->check = 0;
    header->check = openinj_rfc791_checksum((uint8_t *) header, header_len);
}
