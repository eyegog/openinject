#include<netinet/in.h>
#include<unistd.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<string.h>
#include<pcap.h>
#include<openinject.h>
#include<linux/if_ether.h>
#include<linux/if_arp.h>
#include<pthread.h>

uint8_t src_mac[ETH_ALEN], dst_mac[ETH_ALEN];
OPENINJ_CTX *ctx;
pcap_t *handle;

int fatal(char *msg, int rc)
{
    fprintf(stderr, "FATAL: %s\n", msg);
    exit(rc);
}

int main(int argc, char *argv[])
{
    void *send_arp_reqs(void *);
    void *start_listening(void *);

    ctx = openinj_ctx_init();
    int r;

    /* Get the first available IPv4 device */
    for(r = openinj_open_next_dev(ctx); r != -1 &&
            ctx->device_adfamily != AF_INET;)
       ;;

    if(r == -1 && ctx->device_adfamily != AF_INET)
        fatal("No IPv4 device could be found.", r);

    char *str_mac = argv[1]; 
    uint32_t subnet_addr, broadcast_addr, dst_addr;

    struct sockaddr_in *src_addr = (struct sockaddr_in *)ctx->ifa_addr;
    struct sockaddr_in *subnet_mask = (struct sockaddr_in *)ctx->ifa_netmask;
    inet_pton(AF_INET, "255.255.255.255", &broadcast_addr);

    /* Get devices MAC addr */
    r = openinj_get_device_mac(ctx->device_name, src_mac);
    if(r == -1)
        fatal("Couldn't get MAC addr of source device", r);
    r = openinj_macaddr(dst_mac, str_mac);
    if(r == -1)
        fatal("Couldn't convert target MAC addr", r);


    /* Construct the ethernet frame */
    r = openinj_build_ethernet(ctx,
            dst_mac,
            src_mac,
            ETH_P_IP);
    

    unsigned short cidr = 0;
    for(uint32_t tmp = subnet_mask->sin_addr.s_addr; tmp != 0;
            cidr += tmp & 1, tmp = tmp >> 1)
        ;;

    r = openinj_build_ipv4(ctx,
            4,
            5,
            0,
            0,
            0,
            0,
            5,
            0x01,
            src_addr->sin_addr.s_addr,
            broadcast_addr,
            NULL,
            0);

    r = openinj_build_icmpv4(ctx,
            0x08,
            0,
            0x0201);
    openinj_inject(ctx);
}

int compare_macs(uint8_t *mac1, uint8_t *mac2)
{
    for(int i = 0; i < ETH_ALEN; i++)
        if(mac1[i] != mac2[i])
            return -1;
    return 0;
}

void *start_listening(void *args)
{
    char *target_mac = args;
    void listen_for_replys(u_char *target_mac, const struct pcap_pkthdr *ptr,
            const u_char *capture);
    char pcap_errbuf[PCAP_ERRBUF_SIZE];
    struct bpf_program fp;

    handle = pcap_open_live(ctx->device_name, 2048, 1, 2000, pcap_errbuf);
    if(handle == NULL)
        fatal(pcap_errbuf, PCAP_ERROR);

    char filter[100];
    sprintf(filter, "ether src %s and arp and arp [6:2] = 2", target_mac);
    if(pcap_compile(handle, &fp, filter, 0, 0) == PCAP_ERROR)
        fatal(pcap_errbuf, PCAP_ERROR);
    if(pcap_setfilter(handle, &fp) == PCAP_ERROR)
        fatal(pcap_errbuf, PCAP_ERROR);
    pcap_loop(handle, 1, listen_for_replys, dst_mac);

    return NULL;
}

uint16_t print_ethernet_frame(void *hdr, size_t hdr_len)
{
    printf("802.3 Ethernet Frame:\n");
    struct ethhdr *header = hdr;
    printf("  Source Address: ");
    for(int i = 0; i < ETH_ALEN; i++)
        printf("%02X%c", header->h_source[i], (i < ETH_ALEN - 1 ? ':' : '\n'));
    printf("  Destination Address: ");
    for(int i = 0; i < ETH_ALEN; i++)
        printf("%02X%c", header->h_dest[i], (i < ETH_ALEN - 1 ? ':' : '\n'));
    
    uint16_t ethtype = ntohs(header->h_proto);
    printf("  EtherType: 0x%04X\n", ethtype);
    return ethtype;
}

void print_arp_header(void *hdr, size_t hdr_len)
{
    printf("ARP Header:\n");
    struct arp_hdr *header = hdr;
    printf("  Hardware type: %d\n", ntohs(header->htype));
    printf("  Protocol type: 0x%04X\n", ntohs(header->ptype));
    printf("  Hardware Address length: %d\n", header->hlen);
    printf("  Protocol Address length: %d\n", header->plen);
    printf("  Operation: 0x%04X (%s)\n", ntohs(header->op), 
            (header->op == ARPOP_REPLY ? "REPLY" : "REQUEST" ) );
    printf("  Source Hardware Address: ");
    for(int i = 0; i < header->hlen; i++)
        printf("%02X%c", header->sha[i], (i < header->hlen - 1 ? ':' : '\n'));
    printf("  Source Protocol Address: ");
    for(int i = 0; i < header->plen; i++)
        printf("%d%c", header->spa[i], (i < header->plen - 1 ? '.' : '\n'));
    printf("  Target Hardware Address: ");
    for(int i = 0; i < header->hlen; i++)
        printf("%02X%c", header->tha[i], (i < header->hlen - 1 ? ':' : '\n'));
    printf("  Target IP Address: ");
    for(int i = 0; i < header->plen; i++)
        printf("%d%c", header->tpa[i], (i < header->plen - 1 ? '.' : '\n'));
}

void listen_for_replys(u_char *args, const struct pcap_pkthdr *ptr,
        const u_char *capture)
{
    printf("\nGOT REPLY.\n");
    print_ethernet_frame((uint8_t *)capture, ptr->caplen);
    print_arp_header((uint8_t *)capture + ETH_HLEN, ptr->caplen);
    pcap_breakloop(handle);
}
